import math
from typing import Callable
import heapq


def hydra_hercules(
    growth_number: int = 2, heads: list[int] | None = None
) -> int:
    """
    Calculations for a fixed number of `heads` that grow back.
    As default we use the herculean hydra, 9 heads, cut 1 grows 2.
    """
    heads = heads or [3, 9]
    result = 0
    for head_id, head_count in enumerate(heads):
        if not head_id:
            result += head_count
            continue
        result += (
            (1 - growth_number ** (head_id + 1))
            // (1 - growth_number)
            * head_count
        )

    return result


def hydra_rapid_growth(heads: list[int]) -> int:
    """
    Algorithm to solve the problem with a variable number of `heads` that grow

    This algorithm follows these constraints:
    * straight connectors of length y
    * pick right-most leaf
    * growth_number = step you're at
    """
    steps = 0
    hydra = {
        -parent_idx: -children for parent_idx, children in enumerate(heads)
    }

    if len(hydra) == 1:
        steps = -hydra[0]
        hydra[0] = 0

    while hydra[0]:
        q = [(children, parent_idx) for parent_idx, children in hydra.items()]
        heapq.heapify(q)
        # print(f"{hydra = }", f"{steps = :_d}")
        children, parent_idx = heapq.heappop(q)
        # print(f"{parent_idx = }", f"{children = }")

        if parent_idx == 0:
            closest_node = heapq.heappop(q)
            new_children = children - closest_node[0]
            steps -= new_children
            hydra[parent_idx] -= new_children
        else:
            steps += 1
            hydra[parent_idx] += 1
            hydra[parent_idx + 1] -= steps

    return steps


def hydra_rapid_growth_prune(heads: list[int]) -> int:
    """
    Algorithm to solve the problem with a variable number of `heads` that grow

    This algorithm follows these constraints:
    * straight connectors of length y
    * pick right-most leaf
    * growth_number = step you're at
    """
    steps = 0
    hydra = {parent_idx: children for parent_idx, children in enumerate(heads)}

    if len(hydra) == 1:
        steps = hydra[0]
        hydra[0] = 0

    cycles = 0
    while hydra[0]:
        cycles += 1
        if cycles % 10000 == 0:
            print(f"{cycles = :_d}")
            # print(f"{hydra = }", f"{steps = :e}")

        children, parent_idx = max(
            (children, parent_idx) for parent_idx, children in hydra.items()
        )

        children -= 1
        hydra[parent_idx] = children
        steps += 1

        for idx in range(parent_idx - 1, 0, -1):
            hydra[idx] += steps - 1
            steps += 1

        hydra[0] += steps
        removed_children = hydra[0] - hydra[1]
        hydra[0] -= removed_children
        steps += removed_children

    return steps


def hydra_game(
    length: int = 2,
    head_count: int = 9,
    solving_strategy: Callable = hydra_hercules,
) -> int:
    """
    This game tries to cut the heads of a `hydra` in the least number of steps
    possible.

    The hydra is originally a tree, but ultimately the connection to the nodes
    do not really matter, only that we always tackle leaf nodes.
    """
    middle_nodes = math.floor(head_count ** (1 / length))
    heads = [middle_nodes for _ in range(length)]
    heads[-1] = head_count
    return solving_strategy(heads=heads)
