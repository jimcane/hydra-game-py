import pytest

from hydra_game.hydra_game import (
    hydra_game,
    hydra_rapid_growth,
    hydra_rapid_growth_prune,
)


def test_hydra_game_hercules():
    result = hydra_game()
    assert result == 30


@pytest.mark.parametrize(
    "length, expected_result",
    [(1, 1), (2, 3), (3, 11), (4, 983038)],
    # [(1, 1), (2, 3), (3, 11)],
)
def test_rapid_growth(length, expected_result):
    result = hydra_game(
        length=length, head_count=1, solving_strategy=hydra_rapid_growth
    )
    assert result == expected_result


@pytest.mark.parametrize(
    "length, expected_result",
    [(1, 1), (2, 3), (3, 11), (4, 983038)],
    # [(1, 1), (2, 3), (3, 11)],
)
def test_rapid_growth_prune(length, expected_result):
    result = hydra_game(
        length=length, head_count=1, solving_strategy=hydra_rapid_growth_prune
    )
    assert result == expected_result
