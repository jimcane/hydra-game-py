# The Hydra Game

Based on the Numberphile video [The Hydra Game](https://www.youtube.com/watch?v=prURA1i8Qj4).

Steps:

1. Pick leaf x
2. Pick integer n
3. Remove x
4. Add n new branches to x's grandparent

## Math formula

```
d = distance
n = number of extra heads
Wx = number of heads

W1 + (1 - n^2) * W2/1-n  + (1 - n^3) * W3/1-n + ... + (1 - n^d) * W3/1-n
```
